# abc-demo

Demonstration of various features of ABC music notation using abcjs library.

## Introduction

This is a web site which should demonstrate various features available in ABC music notation. It uses the excellent abcjs library for that purpose.

The site is build using Hugo static site generator.

## Development

Start `hugo server -D` from the src directory.

## Deployment

The site is intended to run on GitLab Pages.
The script to generate the output is 

`hugo` from /src directory.
