---
title: Credits
draft: false
---
# Wallpapers

- [Wallpaper Safari](https://wallpapersafari.com/music-score-wallpaper/)
- [Public Domain Pictures](https://www.publicdomainpictures.net/en/view-image.php?image=280708&picture=music-score-sheet-vintage)

# Favicon

- [Free FavIcon](https://www.freefavicon.com/freefavicons/objects/iconinfo/musical-note-2-dennis-b-01r-152-110599.html)

