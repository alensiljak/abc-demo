---
title: Drum Key
subtitle: Shows the basic drumset key
draft: false
tune: |+
  %abc
  X:1
  T:Drum Key
  M:
  L:1/4
  U: k = !style=x!
  K:C clef=perc
  "_Hi-hat Pedal"kD | "_Bass" F | "_Floor tom" A | "_Snare" c | "_Side Stick" k^c | "_High tom" e | "_Ride" kf | "_Hi-hat" kg | "_Crash" ka | "_China/Other" kb |]
---
